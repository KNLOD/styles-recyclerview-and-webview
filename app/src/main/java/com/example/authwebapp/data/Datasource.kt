package com.example.authwebapp.data

import com.example.authwebapp.R
import com.example.authwebapp.model.Link
import com.example.authwebapp.model.RandomStyle

object Datasource {
        val links : List<Link> = listOf(
            Link(R.drawable.google,"Google", "https://www.google.com/"),
            Link(R.drawable.vk,"VK", "https://vk.com/"),
            Link(R.drawable.vibelab,"VibeLab", "https://vibelab.etu.ru/")
        )
    fun getRandomTheme() : RandomStyle {
      return listOf(
          RandomStyle(
              R.style.Theme_second
          ),
          RandomStyle(
                      R.style.Theme_AuthWebApp
          )
        ).random()

    }
}