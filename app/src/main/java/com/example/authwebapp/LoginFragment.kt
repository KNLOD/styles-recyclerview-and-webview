package com.example.authwebapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.authwebapp.data.Datasource
import com.google.android.material.textfield.TextInputEditText

class LoginFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        /**
         * Initializing buttons and inflating View
         */

        val view : View = inflater.inflate(R.layout.fragment_login, container, false)
        val signInButton : Button = view.findViewById(R.id.sign_in)
        val signUpButton : Button = view.findViewById(R.id.sign_up)


        // Navigates to HomeFragment if Sign In button was clicked
        signInButton.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_homeFragment)
        }
        // Navigates to RegistrationFragment if Sign Up button was clicked
        signUpButton.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        return view
    }

}