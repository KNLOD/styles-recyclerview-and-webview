package com.example.authwebapp

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.authwebapp.data.Datasource

class WebFragment : Fragment() {
    private lateinit var webView : WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web, container, false)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){

        val viewModel : SharedWebViewModel by activityViewModels()
        val webView = view.findViewById<WebView>(R.id.web_view)

        webView.webViewClient = object : WebViewClient(){
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                viewModel.setGoBackState(webView.canGoBack())
                if (viewModel.urlLink.value != url) {
                    viewModel.setLink(url!!)
                }
            }
        }
        viewModel.goBack.observe(viewLifecycleOwner, Observer{ goBack ->
            if (goBack == true) {
                webView.goBack()
            }
        })





        viewModel.urlLink.observe(viewLifecycleOwner, Observer{ link ->
            webView.apply{
                loadUrl(link)
                settings.javaScriptEnabled = true
                settings.safeBrowsingEnabled = true
            }
        })






    }
}


