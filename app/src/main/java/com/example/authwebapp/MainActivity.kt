package com.example.authwebapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.authwebapp.data.Datasource
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {
    companion object{
        val LINK = "link"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        /**
         * Initializing NavHost and NavController as well as BottomNavigationView
         */
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav)

        // Init sharedPrefs
        val sharedPreferences = getSharedPreferences("linkSharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        // ViewModel is added to observe changes in WebView
        // Such as Redirecting or saving Link in SharedPreferences
        val viewModel : SharedWebViewModel by viewModels()
        sharedPreferences.getString(LINK, "https://www.google.com")?.let { link ->
            viewModel.setLink(link)
        }

        // If Link is changed -> save it in Shared Prefs
        viewModel.urlLink.observe(this, Observer{ redirectedUrl->
           editor.putString(LINK, redirectedUrl).apply()
        })

        /**
         * Setups BottomNavigationView with NavController
         * in order to navigate and change selected item
         * without implementing onSelectedItemsListeners ourselves
         */
        NavigationUI.setupWithNavController(bottomNavigationView, navController)


        /**
         * Show or hide BottomNavigation in case of changing Destination
         * and change Theme of Application on Navigation
         */
        navController.addOnDestinationChangedListener { _, destination, _ ->

            // Change Theme on Navigation
            val randomTheme = Datasource.getRandomTheme()
            setTheme(randomTheme.themeStyleRes)

            when (destination.id) {
                R.id.homeFragment -> {
                    bottomNavigationView.visibility = View.VISIBLE
                }

                R.id.webFragment -> {
                    bottomNavigationView.visibility = View.VISIBLE
                }

                else -> bottomNavigationView.visibility = View.GONE

            }
        }

    }
    override fun onBackPressed(){
        val viewModel : SharedWebViewModel by viewModels()
        if (viewModel.webViewCanGoBack.value == true){
            viewModel.makeWebViewGoBack(true)

        }else{
            viewModel.makeWebViewGoBack(false)
            super.onBackPressed()
        }

    }



}