package com.example.authwebapp.model

import androidx.annotation.StyleRes

class RandomStyle(
    @StyleRes val themeStyleRes : Int
)